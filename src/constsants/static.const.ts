export class StaticConst {
    public static KILAT_TOKEN = 'KilatToken';
    public static KILAT_VA = 'KilatVa';
    public static KILAT_DEFAULT_CUSTOMER = 'KTMI Customer';

    public static BANK_MANDIRI = 'BMRI';
    public static BANK_BCA = 'BCA';
    public static BANK_BRI = 'BRI';
}