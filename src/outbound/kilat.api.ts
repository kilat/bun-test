import {logger} from "../utils/logger.util.ts";
import type {KilatCallbackReq, KilatLoginReqDto, KilatLoginRespDto, KilatReconDto} from "../dto/kilat-recon.dto.ts";
import {CustomError} from "../exceptions/CustomError.ts";

class KilatApiService {
    private readonly baseUrl = process.env.KILAT_API_URL || '';
    private readonly loginPath = process.env.KILAT_LOGIN_PATH || '';
    private readonly callbackPath = process.env.KILAT_CALLBACK_PATH || '';

    async getAllReconData(): Promise<KilatReconDto[]> {
        try {
            const response = await fetch(`${this.baseUrl}`);
            if (!response.ok) {
                throw new CustomError(`Failed to fetch: ${response.statusText}`, response.status);
            }
            return await response.json();
        } catch (error: any) {
            logger.error(`Error fetching posts: ${error?.message || 'unknown error'}`);
            throw new CustomError(`Failed to fetch: ${error.message}`, 400);
        }
    }

    async login(postData: Partial<KilatLoginReqDto>, xApiKey: string): Promise<KilatLoginRespDto> {
        try {
            const response = await fetch(`${this.baseUrl + this.loginPath}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-api-key': xApiKey
                },
                body: JSON.stringify(postData)
            });
            if (!response.ok) {
                const result = await response.json()
                throw new CustomError(`Failed to login: ${result.result_msg || result.message}`, response.status);
            }
            return await response.json();
        } catch (error: any) {
            logger.error(`Error login: ${error.message || 'unknown error'}`);
            throw new CustomError(`${error.message}`, error.status || 400);
        }
    }

    async callBack(postData: Partial<KilatCallbackReq>, retry: number = 0): Promise<void> {
        try {
            const response = await fetch(`${this.baseUrl + this.callbackPath}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(postData)
            });
            if (!response.ok) {
                logger.error(`[KilatApiService callBack] Failed to send callback: ${response}`);
                if (retry < 3) {
                    logger.error(`[KilatApiService callBack] Retry send callback: ${retry}`);
                    await this.callBack(postData, retry + 1);
                }
                return;
            }
            logger.error(`[KilatApiService callBack] Success send callback: ${response}`);
        } catch (error: any) {
            logger.error(`[KilatApiService callBack] catch error: ${JSON.stringify(error)}`);
        }
    }
}

export default KilatApiService;
