export interface Routes {
    method: string;
    pattern: string;
    handler: any,
    middlewares: any[]
}
