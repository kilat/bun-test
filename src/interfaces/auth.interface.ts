import type {User} from './users.interface.ts';

export interface DataStoredInToken {
    id: string;
}

export interface TokenData {
    token: string;
    expiresIn: number;
}

export interface RequestWithUser extends Request {
    user: User;
}
