import { verify } from 'jsonwebtoken';
import type {RequestWithUser, UsersDto} from "../dto/users.dto.ts";
import {CustomError} from "../exceptions/CustomError.ts";
import UsersService from "../services/users.service.ts";
import type {DataStoredInToken} from "../interfaces/auth.interface.ts";

const userServ = new UsersService();
const authMiddleware = (...roles: string[]) => {
    return async function (req: RequestWithUser, res: Response, next: Function) {
        try {
            const authToken = req.headers.get('Authorization') || null;
            let bearerToken: string | null = null;
            if (authToken) {
                bearerToken = authToken?.split('Bearer ')[1];
            }
            const Authorization = bearerToken;
            if (Authorization) {
                const secretKey: string = Bun.env.SECRET_KEY || 'secret';
                const verificationResponse = verify(Authorization, secretKey) as DataStoredInToken;
                const user = <UsersDto>{
                    id: verificationResponse.id,
                };
                const findUser = await userServ.getById(user)

                if (findUser.id) {
                    req.user = findUser;
                    if (roles.length > 0) {
                        if (!roles.includes(req.user?.id || '')) {
                            next(new CustomError('role not allow', 401));
                        }
                    }
                    next();
                } else {
                    next(new CustomError('Wrong authentication token', 401));
                }
            } else {
                next(new CustomError( 'Authentication token missing', 404));
            }
        } catch (error: any) {
            if (error.message === 'jwt expired') {
                next(new CustomError('Token expired', 400));
            } else {
                next(new CustomError('Wrong authentication token', 401));
            }
        }
    };
}
