import { logger } from "../utils/logger.util.ts";
import type {RequestWithUser} from "../dto/users.dto.ts";

export async function loggerMiddleware(req: Request, next: Function) {
    // console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`);
    logger.info(`${req.method} ${req.url}`);
    return next();
}

export async function jsonParser(req: RequestWithUser, next: Function) {
    const body = await req.json();
    if (req.headers.get("Content-Type")?.includes("application/json")) {
        logger.info(`[jsonParser] Request body: ${JSON.stringify(body)}`);
        req.newBody = body; // Attach parsed body to the request object
    }
    return next();
}