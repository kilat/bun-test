import type {AnySchema} from "yup";
import {CustomError} from "../exceptions/CustomError.ts";
import type {Middleware} from '../../server.ts';
import type {RequestWithUser} from "../dto/users.dto.ts";

export const ValidationMiddleware  = (schema: AnySchema): Middleware => {
    return async (req: RequestWithUser, next: Function): Promise<any> => {
        try {
            await schema.validate(req.newBody);
            return next(); // Proceed to the next middleware or route handler
        } catch (err: any) {
            throw new CustomError(err?.errors[0] || 'Bad Request', 400);
        }
    };
};