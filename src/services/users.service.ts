import Users from "../models/users.model.ts";
import type {UsersDto} from "../dto/users.dto.ts";
import {logger} from "../utils/logger.util.ts";
import {CustomError} from "../exceptions/CustomError.ts";
import {Prisma} from "@prisma/client";


class UsersService {
    usersModel: Users = new Users()

    async getAllByParams(userDto: UsersDto, pageNo = 1, perPage = 10): Promise<UsersDto[]> {
        let page: number = pageNo;
        let rowPerPage = perPage;

        if (pageNo != 1) {
            page = pageNo
        }

        if (rowPerPage != 10) {
            rowPerPage = perPage;
        }

        try {
            return this.usersModel.find(rowPerPage, page, userDto)
        } catch (e) {
            throw new CustomError('failed to fetch user data', 400)
        }
    }

    async getById(userDto: UsersDto): Promise<UsersDto> {
        if (!userDto.id) {
            throw new CustomError(`id cannot empty`, 400);
        }

        try {
            return await this.usersModel.findById(userDto.id);
        } catch (e) {
            throw new CustomError(`User ${userDto.id} Not Found`, 404);
        }
    }

    async addUser(userDto: UsersDto): Promise<UsersDto> {
        try {
            const resp: UsersDto = await this.usersModel.create(userDto);
            logger.info(`logger User created: ${resp}`);
            return resp;
        } catch (e) {
            if (e instanceof Prisma.PrismaClientKnownRequestError) {
                if (e.code === 'P2002') {
                    throw new CustomError(`There is a unique constraint violation, a new user cannot be created with this email`, 400);
                }
            }
            throw new CustomError(`cannot create user data`, 400);
        }

    }

    async editUser(userDto: UsersDto): Promise<UsersDto> {
        try {
            const resp: UsersDto = await this.usersModel.update(userDto);
            logger.info(`logger User updated: ${resp}`);
            return resp;
        } catch (e) {
            if (e instanceof Prisma.PrismaClientKnownRequestError) {
                if (e.code === 'P2025') {
                    throw new CustomError(`user not found`, 404);
                }
            }
            throw new CustomError(`cannot update user data`, 400);
        }
    }

    async deleteUser(userDto: UsersDto): Promise<UsersDto> {
        if (!userDto.id) {
            throw new CustomError(`id cannot empty`, 400);
        }

        try {
            const resp: UsersDto = await this.usersModel.delete(userDto.id);
            logger.info(`logger User deleted: ${resp}`);
            return resp;
        } catch (e) {
            if (e instanceof Prisma.PrismaClientKnownRequestError) {
                if (e.code === 'P2025') {
                    throw new CustomError(`user not found`, 400);
                }
            }
            throw new CustomError(`cannot delete user data`, 400);
        }
    }
}

export default UsersService;