import {logger} from "../utils/logger.util.ts";
import {CustomError} from "../exceptions/CustomError.ts";
import {Prisma} from "@prisma/client";
import VirtualAccountsModel from "../models/virtual-accounts.model.ts";
import type {
    GenerateVaDetailDto,
    GenerateVaRespDto,
    VaInqReqDto,
    VaInqRespDto, VaPayReqDto, VaPayRespDto,
    VirtualAccountsDto
} from "../dto/virtual-accounts.dto.ts";
import type {KilatCallbackReq, KilatLoginReqDto, KilatLoginRespDto} from "../dto/kilat-recon.dto.ts";
import KilatApiService from "../outbound/kilat.api.ts";
import dateUtil from "../utils/date.util.ts";
import RedisService from "../cache/redis-service.js.ts";
import {StaticConst} from "../constsants/static.const.ts";


class VirtualAccountsService {
    virtualAccountsModel: VirtualAccountsModel = new VirtualAccountsModel();
    kilatApiService = new KilatApiService();
    cache = new RedisService();

    async login(kilatLoginReqDto: KilatLoginReqDto, xApiKey: string): Promise<KilatLoginRespDto> {
        try {
            const resp = await this.kilatApiService.login(kilatLoginReqDto, xApiKey);
            logger.info(`logger va created: ${JSON.stringify(resp)}`);

            await this.cache.setWithExpiry(StaticConst.KILAT_TOKEN, resp.data.id_token, 86400);
            logger.info('set cache to ' + StaticConst.KILAT_TOKEN);
            return resp;
        } catch (e: any) {
            logger.error(`Error login: ${e?.message || 'unknown error'}`);
            throw e;
        }

    }

    async getAllByParams(virtualAccountsDto: VirtualAccountsDto, pageNo = 1, perPage = 10): Promise<VirtualAccountsDto[]> {
        let page: number = pageNo;
        let rowPerPage = perPage;

        if (pageNo != 1) {
            page = pageNo
        }

        if (rowPerPage != 10) {
            rowPerPage = perPage;
        }

        try {
            const vaModel = await this.virtualAccountsModel.find(rowPerPage, page, virtualAccountsDto);
            return vaModel as VirtualAccountsDto[];
        } catch (e) {
            throw new CustomError('failed to fetch data', 400)
        }
    }

    async create(virtualAccountsDto: VirtualAccountsDto): Promise<GenerateVaRespDto> {
        try {
            virtualAccountsDto.va_no = this.generateVaNo(virtualAccountsDto);
            virtualAccountsDto.req_data = JSON.stringify(virtualAccountsDto);
            virtualAccountsDto.res_data = JSON.stringify({});
            const resp = await this.virtualAccountsModel.create(virtualAccountsDto);
            logger.info(`logger va created: ${JSON.stringify(resp)}`);

            // not waiting for the cache to be set or loaded
            this.cache.setWithExpiry(
                StaticConst.KILAT_VA + ':' + virtualAccountsDto.va_no,
                JSON.stringify(resp),
                86400
            ).then();

            const detailResp = <GenerateVaDetailDto>{
                tx_id: resp.tx_id,
                channel_type: resp.bank,
                customer_name: resp.customer_name,
                expired_at: this.formatDate(resp.expired_at),
                va_no: resp.va_no,
            }

            // TODO update res_data

            return <GenerateVaRespDto>{
                code: 'STATUS_CREATED',
                message: 'VA created successfully',
                status_code: 201,
                data: detailResp,
            };
        } catch (e) {
            if (e instanceof Prisma.PrismaClientKnownRequestError) {
                if (e.code === 'P2002') {
                    throw new CustomError(`There is a unique constraint violation, a new va cannot be created with this tx_id`, 400);
                }
            }
            logger.error(`Error create va: ${JSON.stringify(e)}`);
            throw new CustomError(`cannot create va data`, 400);
        }

    }

    async inquiry(vaInqReqDto: VaInqReqDto, tokenReq: string): Promise<VaInqRespDto> {

        const vaNo = vaInqReqDto.CompanyCode + vaInqReqDto.CustomerNumber;
        const vaCacheStr = await this.cache.get(StaticConst.KILAT_VA + ':' + vaNo);
        const vaCache: VirtualAccountsDto = JSON.parse(vaCacheStr || '{}');
        const customerName = vaCache.customer_name || StaticConst.KILAT_DEFAULT_CUSTOMER;

        try {
            const validation: {
                result: boolean,
                status: number
            } = await this.inqValidation(vaInqReqDto, tokenReq, vaCache);

            logger.info(`[VirtualAccountsService inquiry] validation: ${JSON.stringify(validation)}`);
            if (!validation.result) {
                return this.setFailedResponse(vaInqReqDto, validation, vaCache);
            }

            const vaReq = <VirtualAccountsDto>{
                id: vaCache.id || '0',
                status: 200,
                request_id: vaInqReqDto.RequestID,
                req_data: JSON.stringify(vaInqReqDto),
                inquiry_at: new Date(),
                updated_at: new Date(),
            }
            const resp = await this.virtualAccountsModel.update(vaReq);
            console.log('resp.total_amount?.toString()', resp.total_amount?.toString())

            // re set cache
            this.cache.setWithExpiry(
                StaticConst.KILAT_VA + ':' + vaNo,
                JSON.stringify(resp),
                86400
            ).then();

            const respRet = <VaInqRespDto>{
                CompanyCode: vaInqReqDto.CompanyCode,
                CustomerNumber: vaInqReqDto.CustomerNumber,
                RequestID: vaInqReqDto.RequestID,
                InquiryStatus: validation.status.toString().slice(-2),
                InquiryReason: this.resultMsg(validation.status),
                CustomerName: customerName || StaticConst.KILAT_DEFAULT_CUSTOMER,
                CurrencyCode: 'IDR',
                TotalAmount: resp.total_amount?.toString() + '.00',
                SubCompany: '00000',
                DetailBills: '',
                AdditionalData: '',
            };

            logger.info(`[VirtualAccountsService inquiry] respRet: ${JSON.stringify(respRet)}`);

            return respRet;
        } catch (e) {
            return this.setFailedResponse(vaInqReqDto, {result: false, status: 5002400}, vaCache);
        }
    }

    async payment(vaPayReqDto: VaPayReqDto, tokenReq: string): Promise<VaPayRespDto> {

        const vaNo = vaPayReqDto.CompanyCode + vaPayReqDto.CustomerNumber;
        const vaCacheStr = await this.cache.get(StaticConst.KILAT_VA + ':' + vaNo);
        const vaCache: VirtualAccountsDto = JSON.parse(vaCacheStr || '{}');
        const customerName = vaCache.customer_name || StaticConst.KILAT_DEFAULT_CUSTOMER;

        try {
            const validation: {
                result: boolean,
                status: number
            } = await this.payValidation(vaPayReqDto, tokenReq, vaCache);

            logger.info(`[VirtualAccountsService payment] validation: ${JSON.stringify(validation)}`);

            if (!validation.result) {
                return this.setFailedPayResponse(vaPayReqDto, validation, vaCache);
            }

            const vaReq = <VirtualAccountsDto>{
                id: vaCache.id || '0',
                status: 309,
                request_id: vaPayReqDto.RequestID,
                req_data: JSON.stringify(vaPayReqDto),
                payment_at: new Date(),
                updated_at: new Date(),
            }
            const resp = await this.virtualAccountsModel.update(vaReq);

            // re set cache
            this.cache.setWithExpiry(
                StaticConst.KILAT_VA + ':' + vaNo,
                JSON.stringify(resp),
                86400
            ).then();

            const kilatCallbackReq: KilatCallbackReq = {
                tx_id: resp.tx_id || '',
                va_no: resp.va_no || '',
                status: 'PAID',
                transaction_date: this.formatDate(new Date())
            }

            // not waiting for the callback to be sent
            this.kilatApiService.callBack(kilatCallbackReq).then()

            const respRet = <VaPayRespDto>{
                CompanyCode: vaPayReqDto.CompanyCode,
                CustomerNumber: vaPayReqDto.CustomerNumber,
                RequestID: vaPayReqDto.RequestID,
                PaymentFlagStatus: validation.status.toString().slice(-2),
                PaymentFlagReason: this.resultMsg(validation.status),
                CustomerName: customerName,
                CurrencyCode: 'IDR',
                TotalAmount: resp.total_amount?.toString() + '.00',
                PaidAmount: resp.total_amount?.toString() + '.00',
                SubCompany: '0000',
                DetailBills: '',
                FreeTexts: '',
                AdditionalData: '',
                TransactionDate: vaPayReqDto.TransactionDate,
            };

            logger.info(`[VirtualAccountsService payment] respRet: ${JSON.stringify(respRet)}`);

            return respRet;
        } catch (e) {
            return this.setFailedPayResponse(vaPayReqDto, {result: false, status: 5002500}, vaCache);
        }
    }

    private generateVaNo(virtualAccountsDto: VirtualAccountsDto): string {
        const length = 8;
        const randomNumber = Math.floor(Math.random() * Math.pow(10, length));

        switch (virtualAccountsDto.bank) {
            case 'BCA':
                return (process.env.OTTO_BCA_PREFIX || '') + (process.env.OTTO_BCA_SUBPREFIX || '') + randomNumber.toString().padStart(length, '0');
            case 'BNI':
                return (process.env.OTTO_BNI_PREFIX || '') + (process.env.OTTO_BNI_SUBPREFIX || '') + randomNumber.toString().padStart(length, '0');
            case 'BRI':
                return (process.env.OTTO_BRI_PREFIX || '') + (process.env.OTTO_BRI_SUBPREFIX || '') + randomNumber.toString().padStart(length, '0');
            case 'BMRI':
                return (process.env.OTTO_MANDIRI_PREFIX || '') + (process.env.OTTO_MANDIRI_SUBPREFIX || '') + randomNumber.toString().padStart(length, '0');
            case 'MANDIRI':
                return (process.env.OTTO_MANDIRI_PREFIX || '') + (process.env.OTTO_MANDIRI_SUBPREFIX || '') + randomNumber.toString().padStart(length, '0');
            default:
                return '00000000';
        }
    }

    private formatDate(date: Date | null | undefined): string {
        if (!date) {
            date = new Date();
        }

        const year = date?.getFullYear();
        const month = String(date?.getMonth() + 1).padStart(2, '0'); // Month starts from 0
        const day = String(date?.getDate()).padStart(2, '0');
        const hours = String(date?.getHours()).padStart(2, '0');
        const minutes = String(date?.getMinutes()).padStart(2, '0');
        const seconds = String(date?.getSeconds()).padStart(2, '0');

        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    }

    private async inqValidation(vaInqReqDto: VaInqReqDto, tokenReq: string, vaCache: VirtualAccountsDto):
        Promise<{ result: boolean, status: number }> {
        logger.info(`[VirtualAccountsService inqValidation] start`);

        let bearerToken: string = tokenReq?.split('Bearer ')[1];
        const token = await this.cache.get(StaticConst.KILAT_TOKEN) || '123';

        if (token !== bearerToken) {
            logger.info(`[VirtualAccountsService inqValidation] token not match`);
            // return {result: false, status: 4012401};
        }

        try {
            const vaModel = vaCache;
            if (vaModel.va_no !== vaInqReqDto.CompanyCode + vaInqReqDto.CustomerNumber) {
                return {result: false, status: 4042412};
            }

            if (vaModel.status === 309) {
                return {result: false, status: 4042412};
            }

            if (vaInqReqDto.ChannelType === StaticConst.BANK_MANDIRI) {
                if (vaInqReqDto.CompanyCode !== process.env.OTTO_MANDIRI_PREFIX) {
                    return {result: false, status: 4002400};
                }
            }

            if (vaInqReqDto.ChannelType === StaticConst.BANK_BCA) {
                if (vaInqReqDto.CompanyCode !== process.env.OTTO_BCA_PREFIX) {
                    return {result: false, status: 4002400};
                }
            }

            if (vaInqReqDto.ChannelType === StaticConst.BANK_BRI) {
                if (vaInqReqDto.CompanyCode !== process.env.OTTO_BRI_PREFIX) {
                    return {result: false, status: 4002400};
                }
            }

            if (vaModel.expired_at) {
                if (new Date(vaModel.expired_at) < dateUtil.StringToDate(vaInqReqDto.TransactionDate)) {
                    logger.info(`[VirtualAccountsService inqValidation] va expired`);
                    return {result: false, status: 4002400};
                } else {
                    return {result: true, status: 2002400};
                }
            } else {
                return {result: false, status: 4002400};
            }
        } catch (e) {
            return {result: false, status: 5002400};
        }
    }

    private async payValidation(vaPayReqDto: VaPayReqDto, tokenReq: string, vaCache: VirtualAccountsDto):
        Promise<{ result: boolean, status: number }> {
        logger.info(`[VirtualAccountsService payValidation] start`);

        let bearerToken: string = tokenReq?.split('Bearer ')[1];
        const token = await this.cache.get(StaticConst.KILAT_TOKEN) || '123';

        if (token !== bearerToken) {
            logger.info(`[VirtualAccountsService payValidation] token not match`);
            // return {result: false, status: 4012501};
        }

        try {
            const vaModel = vaCache;
            if (vaModel.va_no !== vaPayReqDto.CompanyCode + vaPayReqDto.CustomerNumber) {
                return {result: false, status: 4042512};
            }

            if (vaModel.status !== 200) {
                if (vaModel.status === 309) {
                    return {result: false, status: 4042514};
                }
                return {result: false, status: 5002500};
            }

            if (Number(vaPayReqDto.TotalAmount) !== vaModel.total_amount) {
                return {result: false, status: 4042513};
            }

            if (vaModel.expired_at) {
                console.log('vaModel.expired_at', new Date(vaModel.expired_at))
                console.log('dateUtil.StringToDate(vaPayReqDto.TransactionDate)', dateUtil.StringToDate(vaPayReqDto.TransactionDate))
                if (new Date(vaModel.expired_at) < dateUtil.StringToDate(vaPayReqDto.TransactionDate)) {
                    logger.info(`[VirtualAccountsService payValidation] va expired`);
                    return {result: false, status: 4032500};
                } else {
                    return {result: true, status: 2002500};
                }
            } else {
                return {result: false, status: 4002500};
            }
        } catch (e) {
            return {result: false, status: 5002500};
        }
    }

    private resultMsg(statusCd: number): { Indonesian: string; English: string; } {
        if (statusCd === 2002400) {
            return {Indonesian: 'Sukses', English: 'Success Inquiry'};
        } else if (statusCd === 4042412) {
            return {Indonesian: 'VA tidak ditemukan', English: 'VA not found'};
        } else if (statusCd === 4002400) {
            return {Indonesian: 'Request Salah', English: 'Bad request'};
        } else if (statusCd === 5002400) {
            return {Indonesian: 'Server dalam error', English: 'Internal Server Error '};
        } else if (statusCd === 4012401) {
            return {Indonesian: 'Token Salah', English: 'Invalid Token'};
        } else if (statusCd === 2002500) {
            return {Indonesian: 'Pembayaran berhasil', English: 'Success Payment '};
        } else if (statusCd === 4042512) {
            return {Indonesian: 'No billing salah', English: 'Invalid Bill'};
        } else if (statusCd === 4032500) {
            return {Indonesian: 'Transaksi Kadaluarsa', English: 'Transaction Expired'};
        } else if (statusCd === 4012501) {
            return {Indonesian: 'Token Salah', English: 'Invalid Token'};
        } else if (statusCd === 4002500) {
            return {Indonesian: 'Request Salah', English: 'Bad request'};
        } else if (statusCd === 5002500) {
            return {Indonesian: 'Server dalam error', English: 'Internal Server Error '};
        } else if (statusCd === 4042513) {
            return {Indonesian: 'Nominal Salah', English: 'Invalid Amount'};
        } else if (statusCd === 4042514) {
            return {Indonesian: 'Tagihan sudah lunas', English: 'Paid Bill '};
        }

        return {Indonesian: 'Inquiry Berhasil', English: 'Success Inquiry'};
    }

    private async setFailedResponse(vaInqReqDto: VaInqReqDto, validation: {
        result: boolean,
        status: number
    }, vaCache: VirtualAccountsDto): Promise<VaInqRespDto> {
        const customerName = vaCache.customer_name || StaticConst.KILAT_DEFAULT_CUSTOMER;
        return <VaInqRespDto>{
            CompanyCode: vaInqReqDto.CompanyCode,
            CustomerNumber: vaInqReqDto.CustomerNumber,
            RequestID: vaInqReqDto.RequestID,
            InquiryStatus: validation.status.toString().slice(-2),
            InquiryReason: this.resultMsg(validation.status),
            CustomerName: customerName,
            CurrencyCode: 'IDR',
            TotalAmount: '00',
            SubCompany: '0000',
            DetailBills: null,
            FreeTexts: '',
            AdditionalData: '',
        }
    }

    private async setFailedPayResponse(vaPayReqDto: VaPayReqDto, validation: {
        result: boolean,
        status: number
    }, vaCache: VirtualAccountsDto): Promise<VaPayRespDto> {
        const customerName = vaCache.customer_name || StaticConst.KILAT_DEFAULT_CUSTOMER;
        return <VaPayRespDto>{
            CompanyCode: vaPayReqDto.CompanyCode,
            CustomerNumber: vaPayReqDto.CustomerNumber,
            RequestID: vaPayReqDto.RequestID,
            PaymentFlagStatus: validation.status.toString().slice(-2),
            PaymentFlagReason: this.resultMsg(validation.status),
            CustomerName: customerName,
            CurrencyCode: 'IDR',
            TotalAmount: '00',
            PaidAmount: '00',
            SubCompany: '0000',
            DetailBills: null,
            FreeTexts: '',
            AdditionalData: '',
            TransactionDate: vaPayReqDto.TransactionDate,
        }
    }
}

export default VirtualAccountsService;
