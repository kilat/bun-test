import Redis from 'ioredis';
import {logger} from "../utils/logger.util.ts";

class RedisService {
    redis: Redis;

    constructor() {
        logger.info('Connecting to Redis...');
        logger.info(process.env.REDIS_HOST);
        this.redis = new Redis({
            host: process.env.REDIS_HOST,
            port: Number(process.env.REDIS_PORT || 6379),
            // password: Bun.env.REDIS_PORT || '',
            db: Number(process.env.REDIS_DB || 2),
            // username: Bun.env.REDIS_USERNAME || '',
        });
    }

    async set(key: string, value: string) {
        try {
            await this.checkExists(key);
            await this.redis.set(key, value);
        } catch (err) {
            logger.error('Error setting key with expiry: ' + JSON.stringify(err));
        }
    }

    async setWithExpiry(key: string, value: string, expiryInSeconds: number) {
        try {
            await this.checkExists(key);
            await this.redis.set(key, value, 'EX', expiryInSeconds);
        } catch (err) {
            logger.error('Error setting key with expiry:' + JSON.stringify(err));
        }
    }

    async get(key: string): Promise<string | null> {
        try {
            return await this.redis.get(key);
        } catch (err) {
            console.error('Error getting key:', err);
            return null;
        }
    }

    async checkExists(key: string) {
        const exists = await this.redis.exists(key);
        if (exists) {
            // Delete the existing key
            await this.redis.del(key);
            logger.info(`Deleted existing key: ${key}`);
        }
    }

    disconnect() {
        this.redis.disconnect();
    }
}

// const redisService = new RedisService();
export default RedisService;