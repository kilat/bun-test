import type {RequestWithUser, UsersDto} from "../dto/users.dto.ts";
import UsersService from "../services/users.service.ts";
import {logger} from "../utils/logger.util.ts";

const userService = new UsersService();
class UsersController {
    // async parseJsonBody(req: Request) {
    //     const text = await req.text();
    //     return JSON.parse(text);
    // }

    async getAllUsers(req: Request, _param: any): Promise<Response> {
        logger.info(`[UserController getAllUsers] start`);

        const url = new URL(req.url);
        logger.info(`[UserController getAllUsers] Request body: ${JSON.stringify(url.searchParams)}`);
        const userReq: UsersDto = {
            name: url.searchParams.get('name'),
            email: url.searchParams.get('email') || '',
        }

        const pageNo = Number(url.searchParams.get('pageNo') || 1);
        const pageSize = Number(url.searchParams.get('pageSize') || 10);
        const userResp = await userService.getAllByParams(userReq, pageNo, pageSize)
        return new Response(JSON.stringify(userResp), {status: 200, headers: {"Content-Type": "application/json"}});
    }

    async getUserById(_req: Request, param: any): Promise<Response> {
        logger.info(`[UserController getUserById] start`);
        const userReq: UsersDto = {
            id: param.id || '',
        }
        if (userReq.id) {
            try {
                const userResp = await userService.getById(userReq)
                return new Response(JSON.stringify(userResp), {status: 200, headers: {"Content-Type": "application/json"}});
            } catch (error: any) {
                logger.error(`[UserController getUserById] error: ${error}`);
                return new Response(JSON.stringify(error), {status: error?.status, headers: {"Content-Type": "application/json"}});
            }
        } else {
            return new Response("User Not Found", {status: 404});
        }
    }

    async createUser(req: RequestWithUser): Promise<Response> {
        logger.info(`[UserController createUser] start`);

        const body = req?.newBody || {};
        logger.info(`[UserController CreateUser] Request body: ${JSON.stringify(body)}`);
        const userDto = <UsersDto>{
            name: body?.name,
            email: body?.email,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        try {
            const newUser: UsersDto = await userService.addUser(userDto);
            return new Response(JSON.stringify(newUser), {status: 201, headers: {"Content-Type": "application/json"}});
        } catch (error: any) {
            logger.error(`[UserController CreateUser] error: ${error}`);
            return new Response(JSON.stringify(error), {status: error?.status, headers: {"Content-Type": "application/json"}});
        }
    }

    async updateUser(req: RequestWithUser, params: any): Promise<Response> {
        logger.info(`[UserController updateUser] start`);

        const body = req?.newBody || {};
        logger.info(`[UserController updateUser] Request body: ${JSON.stringify(body)}`);

        const userDto = <UsersDto>{
            id: params.id || '',
            name: body?.name,
            email: body?.email,
            updatedAt: new Date()
        }

        try {
            const editUser: UsersDto = await userService.editUser(userDto);
            return new Response(JSON.stringify(editUser), {status: 201, headers: {"Content-Type": "application/json"}});
        } catch (error: any) {
            logger.error(`[UserController CreateUser] error: ${error}`);
            return new Response(JSON.stringify(error), {status: error?.status, headers: {"Content-Type": "application/json"}});
        }
    }

    async deleteUser(_req: Request, params: any): Promise<Response> {
        logger.info(`[UserController deleteUser] start`);

        logger.info(`[UserController CreateUser] Request params: ${JSON.stringify(params)}`);
        const userDto = <UsersDto>{
            id: params.id || '',
        }

        try {
            const editUser: UsersDto = await userService.deleteUser(userDto);
            return new Response(JSON.stringify(editUser), {status: 201, headers: {"Content-Type": "application/json"}});
        } catch (error: any) {
            logger.error(`[UserController CreateUser] error: ${error}`);
            return new Response(JSON.stringify(error), {
                status: error?.status,
                headers: {"Content-Type": "application/json"}
            });
        }
    }
}

export default UsersController;