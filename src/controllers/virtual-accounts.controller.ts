import type { RequestWithUser } from "../dto/users.dto.ts";
import { logger } from "../utils/logger.util.ts";
import VirtualAccountsService from "../services/virtual-accounts.service.ts";
import type {GenerateVaRespDto, VaInqRespDto, VaPayRespDto, VirtualAccountsDto} from "../dto/virtual-accounts.dto.ts";
import dateUtil from "../utils/date.util.ts";
import type {KilatLoginReqDto, KilatLoginRespDto} from "../dto/kilat-recon.dto.ts";
import General from "../utils/general.util.ts";

class VirtualAccountsController {

    private virtualAccountsService = new VirtualAccountsService();
    private general = new General();

    login = async (req: RequestWithUser): Promise<Response | void> => {
        logger.info(`[VirtualAccountsController login] start`);

        const xApiKey = req?.headers.get('x-api-key');
        if (!xApiKey) {
            return this.general.Resp(JSON.stringify({ message: 'x-api-key is required' }), 400);
        }

        const body = req?.newBody || {};
        logger.info(`[VirtualAccountsController login] create body: ${JSON.stringify(body)}`);
        const loginReq = <KilatLoginReqDto>{
            password: body?.password || '',
            username: body?.username || '',
        };

        try {
            const loginResp: KilatLoginRespDto = await this.virtualAccountsService.login(loginReq, xApiKey);
            return this.general.Resp(JSON.stringify(loginResp), 200);
        } catch (error: any) {
            logger.error(`[VirtualAccountsController create] error: ${error.message}`);
            return this.general.Resp(JSON.stringify(error), error.status || 500);
        }
    };

    getAll = async (req: Request, _param: any): Promise<Response> => {
        logger.info(`[VirtualAccountsController getAll] start`);

        const url = new URL(req.url);
        logger.info(`[VirtualAccountsController getAll] Request body: ${JSON.stringify(url.searchParams)}`);
        const vaDto: VirtualAccountsDto = {
            tx_id: url.searchParams.get('tx_id') || '',
        };

        const pageNo = Number(url.searchParams.get('pageNo') || 1);
        const pageSize = Number(url.searchParams.get('pageSize') || 10);
        const userResp = await this.virtualAccountsService.getAllByParams(vaDto, pageNo, pageSize);
        return new Response(JSON.stringify(userResp), {status: 200, headers: {"Content-Type": "application/json"}});
    };

    create = async (req: RequestWithUser): Promise<Response | void> => {
        logger.info(`[VirtualAccountsController create] start`);

        const body = req?.newBody || {};
        logger.info(`[VirtualAccountsController create] create body: ${JSON.stringify(body)}`);
        const va = <VirtualAccountsDto>{
            tx_id: body?.tx_id || '',
            status: 100,
            expired_at: dateUtil.endOfDayFromTimestamp(),
            created_at: new Date(),
            bank: body?.channel_type || '',
            additional_data: body?.additional_data || '',
            customer_name: body?.customer_name || '',
            total_amount: body?.total_amount || 0,
        };

        try {
            const generateVa: GenerateVaRespDto = await this.virtualAccountsService.create(va);
            return new Response(JSON.stringify(generateVa), {
                status: 201,
                headers: {"Content-Type": "application/json"}
            });
        } catch (error: any) {
            logger.error(`[VirtualAccountsController create] error: ${error.message}`);
            return new Response(JSON.stringify({
                message: error.message,
                status: error.status
            }), {status: error.status || 500, headers: {"Content-Type": "application/json"}});
        }
    };

    inquiry = async (req: RequestWithUser): Promise<Response | void> => {
        logger.info(`[VirtualAccountsController create] inquiry`);

        const token = req?.headers.get('Authorization');
        if (!token) {
            logger.info(`[VirtualAccountsController inquiry] header Authorization not provided`)
            return this.general.Resp(JSON.stringify({ message: 'token is required' }), 400);
        }

        const body = req?.newBody || {};
        logger.info(`[VirtualAccountsController inquiry] inquiry body: ${JSON.stringify(body)}`);

        try {
            const inqVa: VaInqRespDto = await this.virtualAccountsService.inquiry(body, token);
            logger.info(`[VirtualAccountsController inquiry] inquiry response: ${JSON.stringify(inqVa)}`);
            return new Response(JSON.stringify(inqVa), {
                status: 200,
                headers: {"Content-Type": "application/json"}
            });
        } catch (error: any) {
            logger.error(`[VirtualAccountsController create] error: ${error.message}`);
            return new Response(JSON.stringify({
                message: error.message,
                status: error.status
            }), {status: error.status || 500, headers: {"Content-Type": "application/json"}});
        }
    };

    payment = async (req: RequestWithUser): Promise<Response | void> => {
        logger.info(`[VirtualAccountsController payment] inquiry`);

        const token = req?.headers.get('Authorization');
        if (!token) {
            return this.general.Resp(JSON.stringify({ message: 'token is required' }), 400);
        }

        const body = req?.newBody || {};
        logger.info(`[VirtualAccountsController payment] payment body: ${JSON.stringify(body)}`);

        try {
            const payVa: VaPayRespDto = await this.virtualAccountsService.payment(body, token);
            logger.info(`[VirtualAccountsController payment] payment response: ${JSON.stringify(payVa)}`);
            return new Response(JSON.stringify(payVa), {
                status: 200,
                headers: {"Content-Type": "application/json"}
            });
        } catch (error: any) {
            logger.error(`[VirtualAccountsController payment] error: ${error.message}`);
            return new Response(JSON.stringify({
                message: error.message,
                status: error.status
            }), {status: error.status || 500, headers: {"Content-Type": "application/json"}});
        }
    };
}

export default VirtualAccountsController;