export class CustomError extends Error {
    status: number;

    constructor(message: string, status: number = 500) {
        super(message);
        this.status = status;
    }

    toJSON() {
        return {
            message: this.message,
            status: this.status
        };
    }
}