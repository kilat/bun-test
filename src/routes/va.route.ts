import { loggerMiddleware, jsonParser } from "../middleware/middleware.ts";
import type { Server } from "../../server.ts";
import VirtualAccountsController from "../controllers/virtual-accounts.controller.ts";
import { ValidationMiddleware } from "../middleware/validator.middleware.ts";
import { GenerateVaReqSchema } from "../dto/virtual-accounts.dto.ts";
import {KilatLoginReqSchema} from "../dto/kilat-recon.dto.ts"; // Import the Server type

export function vaRoutes(server: Server) {
    const vaController = new VirtualAccountsController();

    server.addRoute("GET", "/va", vaController.getAll, [loggerMiddleware]);
    server.addRoute("POST", "/v1/va/login", vaController.login, [loggerMiddleware, jsonParser, ValidationMiddleware(KilatLoginReqSchema)]);
    server.addRoute("POST", "/v1/va/inquiry", vaController.inquiry, [loggerMiddleware, jsonParser]);
    server.addRoute("POST", "/v1/va/payment", vaController.payment, [loggerMiddleware, jsonParser]);
    server.addRoute("POST", "/v1/va", vaController.create, [loggerMiddleware, jsonParser, ValidationMiddleware(GenerateVaReqSchema)]);
}