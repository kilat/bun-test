import UsersController from '../controllers/users.controller.ts';
import { loggerMiddleware, jsonParser } from "../middleware/middleware.ts";
import type { Server } from "../../server.ts"; // Import the Server type

export function userRoutes(server: Server) {
    const usersController = new UsersController();

    server.addRoute("GET", "/users/:id", usersController.getUserById, [loggerMiddleware]);
    server.addRoute("GET", "/users", usersController.getAllUsers, [loggerMiddleware]);
    server.addRoute("POST", "/users", usersController.createUser, [loggerMiddleware, jsonParser]);
    server.addRoute("PUT", "/users/:id", usersController.updateUser, [loggerMiddleware, jsonParser]);
    server.addRoute("DELETE", "/users/:id", usersController.deleteUser, [loggerMiddleware]);
}