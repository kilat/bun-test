import {PrismaClient} from '@prisma/client';
import type {UsersDto} from '../dto/users.dto.ts';
class User {
    prisma = new PrismaClient({
        log: ['query', 'info', 'warn', 'error'],
    });

    // constructor(private prisma: PrismaClient) {}

    async find(rowPerPage: number, page: number, params: UsersDto): Promise<UsersDto[]> {
        return this.prisma.user.findMany({
            where: this.where(params),
            skip: page - 1,
            take: rowPerPage,
            orderBy: [{
                name: 'asc',
            }, {
                email: 'asc',
            }]
        });
    }

    async findById(id: string): Promise<UsersDto> {
        return this.prisma.user.findFirstOrThrow({
            where: {
                id: id,
            }
        });
    }

    async create(usersDto: UsersDto): Promise<UsersDto> {
        return this.prisma.user.create({
            data: this.where(usersDto),
        });
    }

    async update(usersDto: UsersDto): Promise<UsersDto> {
        return this.prisma.user.update({
            where: {
                id: usersDto.id,
            },
            data: this.where(usersDto),
        });
    }

    async delete(id: string): Promise<UsersDto> {
        return this.prisma.user.delete({
            where: {
                id: id,
            }
        });
    }

    async count(params: UsersDto) {
        return this.prisma.user.count({
            where: this.where(params),
        });
    }

    where(params: UsersDto): any {
        const where: any = {};
        if (params.name) {
            where.name = params.name;
        }

        if (params.email) {
            where.email = params.email;
        }

        if (params.createdAt) {
            where.createdAt = params.createdAt;
        }

        if (params.updatedAt) {
            where.updatedAt = params.updatedAt;
        }

        return where;
    }
}


export default User;