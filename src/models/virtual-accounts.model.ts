import {PrismaClient} from "@prisma/client";
import type {VirtualAccountsDto} from "../dto/virtual-accounts.dto.ts";

class VirtualAccountsModel {
    prisma = new PrismaClient({
        // log: ['query', 'info', 'warn', 'error'],
    });

    async find(rowPerPage: number, page: number, params: VirtualAccountsDto): Promise<VirtualAccountsInterface[]> {
        return this.prisma.virtualAccounts.findMany({
            where: this.where(params),
            skip: page - 1,
            take: rowPerPage,
            orderBy: [{
                payment_at: 'desc',
            }]
        });
    }

    async findByParams(params: VirtualAccountsDto): Promise<VirtualAccountsInterface> {
        return this.prisma.virtualAccounts.findFirstOrThrow({
            where: this.where(params),
        });
    }

    async create(params: VirtualAccountsDto): Promise<VirtualAccountsInterface> {
        return this.prisma.virtualAccounts.create({
            data: this.where(params),
        });
    }

    async update(params: VirtualAccountsDto): Promise<VirtualAccountsInterface> {
        console.log(params.id)
        return this.prisma.virtualAccounts.update({
            where: {
                id: params.id,
            },
            data: this.where(params),
        });
    }

    where(params: VirtualAccountsDto): any {
        const where: VirtualAccountsInterface = {};
        if (params.tx_id) {
            where.tx_id = params.tx_id;
        }

        if (params.va_no) {
            where.va_no = params.va_no;
        }

        if (params.expired_at) {
            where.expired_at = params.expired_at;
        }

        if (params.status) {
            where.status = params.status;
            if (params.status === 1000) {
                // @ts-ignore
                where.status = {
                    gte: 100,
                    lte: 308,
                }
            }
        }

        if (params.bank) {
            where.bank = params.bank;
        }

        if (params.total_amount) {
            where.total_amount = params.total_amount;
        }

        if (params.inquiry_at) {
            where.inquiry_at = params.inquiry_at;
        }

        if (params.payment_at) {
            where.payment_at = params.payment_at;
        }

        if (params.created_at) {
            where.created_at = params.created_at;
        }

        if (params.updated_at) {
            where.updated_at = params.updated_at;
        }

        if (params.deleted_at) {
            where.deleted_at = params.deleted_at;
        }

        if (params.request_id) {
            where.request_id = params.request_id;
        }

        if (params.customer_name) {
            where.customer_name = params.customer_name;
        }

        if (params.req_data) {
            where.req_data = params.req_data;
        }

        if (params.res_data) {
            where.res_data = params.res_data;
        }

        return where;
    }

}

interface VirtualAccountsInterface {
    id?: string;
    tx_id?: string;
    va_no?: string | null;
    request_id?: string | null;
    customer_name?: string | null;
    bank?: string | null;
    additional_data?: string | null;
    req_data?: string | null;
    res_data?: string | null;
    total_amount?: number | null;
    status?: number | null;
    expired_at?: Date | null;
    inquiry_at?: Date | null;
    payment_at?: Date | null;
    created_at?: Date | null;
    updated_at?: Date | null;
    deleted_at?: Date | null;
}

export default VirtualAccountsModel;