
class DateUtil {
    static startOfDayFromTimestamp(ts: number): Date {
        // Create a Date object from the timestamp
        const date = new Date(ts);

        // Set time to 00:00:00.000 (first millisecond of the day)
        date.setHours(0, 0, 0, 0);

        return date;
    }

    static endOfDayFromTimestamp(): Date {
        // Create a Date object from the timestamp
        const date = new Date();

        // Set time to 23:59:59.999 (last millisecond of the day)
        date.setHours(23, 59, 59, 999);

        return date;
    }

    static StringToDate(dateString: string): Date {
        const [datePart, timePart] = dateString.split(' ');
        const [day, month, year] = datePart.split('/');
        const [hours, minutes, seconds] = timePart.split(':');

        return new Date(Number(year), Number(month) - 1, Number(day), Number(hours), Number(minutes), Number(seconds));
    }
}

export default DateUtil;