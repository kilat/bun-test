class General {
    Resp(body: string, status: number): Response {
        return new Response(body, {status: status || 500, headers: {"Content-Type": "application/json"}});
    }
}

export default General;