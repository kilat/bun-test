import * as yup from "yup";

export interface KilatReconDto {
    id?: string;
}

export interface KilatLoginReqDto {
    username: string;
    password: string;
}

export interface KilatLoginRespDto {
    meta: KilatLoginRespMetaDto;
    data: KilatLoginRespDataDto;
}

export interface KilatLoginRespMetaDto {
    code: number;
    message: string;
    status: boolean;
}

export interface KilatLoginRespDataDto {
    username: string
    id_token: string
    id: string;
}

export const KilatLoginReqSchema = yup.object({
    username: yup.string().required(),
    password: yup.string().required(),
});

export interface KilatCallbackReq {
    tx_id: string;
    va_no: string;
    status: string;
    transaction_date: string;
}