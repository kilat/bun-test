export interface UsersDto {
    id?: string;
    name?: string | null;
    email?: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface RequestWithUser extends Request {
    newBody: any;
    user: UsersDto;
}