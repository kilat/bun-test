import * as yup from 'yup';

export interface VirtualAccountsDto {
    id?: string;
    tx_id?: string;
    va_no?: string;
    request_id?: string;
    customer_name?: string;
    bank?: string;
    additional_data?: string;
    req_data?: string;
    res_data?: string;
    total_amount?: number;
    status?: number;
    expired_at?: Date;
    inquiry_at?: Date;
    payment_at?: Date;
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}


export interface GenerateVaRequestDto {
    total_amount: number;
    tx_id: string;
    channel_type: string;
    additional_data?: string;
    customer_name: string;
}

export interface GenerateVaRespDto {
    data: GenerateVaDetailDto;
    message: string;
    code: string;
    status_code: number;
}

export interface GenerateVaDetailDto {
    tx_id: string;
    va_no: string;
    expired_at: string;
    channel_type: string;
    customer_name: string
}

export const GenerateVaReqSchema = yup.object({
    tx_id: yup.string().required(),
    channel_type: yup.string().required(),
    customer_name: yup.string().required(),
    // email: yup.string().email().required(),
    total_amount: yup.number().positive().integer().required(),
});


////////////////////// VA INQ /////////////////////////

export interface VaInqReqDto {
    CompanyCode: string;
    CustomerNumber: string;
    RequestID: string;
    ChannelType: string;
    TransactionDate: string;
    AdditionalData?: string;
}

export interface VaInqRespDto {
    CompanyCode: string;
    CustomerNumber: string;
    RequestID: string;
    InquiryStatus: string;
    InquiryReason: { Indonesian: string; English: string; };
    CustomerName: string;
    CurrencyCode: string;
    TotalAmount: string;
    SubCompany: string;
    DetailBills?: string | null;
    AdditionalData?: string;
    FreeTexts?: string;
}

////////////////////////// VA PAY /////////////////////////

export interface VaPayReqDto {
    CompanyCode: string;
    CustomerNumber: string;
    RequestID: string;
    ChannelType: string;
    TransactionDate: string;
    AdditionalData?: string;
    CustomerName: string;
    CurrencyCode: string;
    PaidAmount: string;
    TotalAmount: string;
    SubCompany: string;
    Reference: string;
    DetailBills: [],
}

export interface VaPayRespDto {
    CompanyCode: string;
    CustomerNumber: string;
    RequestID: string;
    PaymentFlagStatus: string;
    PaymentFlagReason: {
        Indonesian: string;
        English:string;
    },
    CustomerName: string;
    CurrencyCode: string;
    PaidAmount: string;
    TotalAmount: string;
    TransactionDate: string;
    DetailBills?: string | null;
    FreeTexts?: string;
    AdditionalData?: string;
    SubCompany: string;
}