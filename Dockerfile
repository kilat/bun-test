# Stage 1: Build
FROM oven/bun AS build

ENV NODE_ENV=production

WORKDIR /app

# Copy essential files first to leverage Docker cache
COPY bun.lockb package.json /app/

# Copy the rest of the application files
COPY . /app/
COPY .env .env.dev .env.production /app/
COPY prisma /app/prisma

# Install dependencies and generate Prisma client
RUN bun install
RUN bunx prisma generate

# Build the application to a binary file
RUN bun build ./index.ts --compile --outfile cli

# Stage 2: Production
FROM ubuntu:22.04

# Install necessary libraries
RUN apt-get update -y && apt-get install -y openssl

ENV NODE_ENV=production

WORKDIR /app

# Copy the compiled binary and necessary files from the build stage
COPY --from=build /app/cli /app/cli
COPY --from=build /app/node_modules/.prisma /app/node_modules/.prisma
COPY --from=build /app/node_modules/.bin /app/node_modules/.bin
COPY --from=build /app/node_modules/prisma /app/node_modules/prisma

# Copy the environment files if needed
COPY .env .env.dev .env.production /app/

# Set the entry point to the compiled binary
CMD ["/app/cli"]

# Expose the application port
EXPOSE 3102




#FROM oven/bun
#
#WORKDIR /app
#
#COPY bun.lockb /app/.
#COPY package.json /app/.
#COPY ./.env /app/.
#COPY ./.env.dev /app/.
#COPY ./.env.production /app/.
#COPY prisma /app/prisma
#
#ENV NODE_ENV=production
#
## Install dependencies
#RUN bun install
#RUN bunx prisma generate
#
#COPY . /app/.
#
#CMD ["bun", "index.ts"]

# docker build --platform linux/amd64 --tag ariandin1411/va-service-be-2:0.1 .
# docker push ariandin1411/va-service-be-2:0.1

# docker run -d --network ktmi --restart always -p 3102:3102 --name va-service-be-new ariandin1411/va-service-be-2:0.1