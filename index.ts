import {userRoutes} from "./src/routes/users.route.ts";
import {vaRoutes} from "./src/routes/va.route.ts";
import {logger} from "./src/utils/logger.util.ts";
import {Server} from "./server.ts";

console.log('Server started');
const server = new Server(Number(process.env.PORT) || 3000);

// Define routes
userRoutes(server);
vaRoutes(server);

server.start();

logger.info(`=================================`);
logger.info(`======= ENV: ${process.env.NODE_ENV || 'development'} =======`);
logger.info('v.1.0.0');
logger.info(`🚀 App listening on the port ${Number(process.env.PORT) || 3000}`);
logger.info(`=================================`);