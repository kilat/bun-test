const passwordHash = await Bun.password.hash("passwordSecret", {
    algorithm: "bcrypt",
    cost: 10,
});

const result = await Bun.password.verify("passwordSecret", passwordHash, 'bcrypt');
console.log(result); // true