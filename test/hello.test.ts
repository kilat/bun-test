import { describe, it, expect } from 'bun:test';
import {sayHello} from "../src/hello.ts";

describe('Bun Test Runner', () => {
    it('should support unit test', async () => {
        const resp = sayHello('Ari');
        expect(resp).toBe('Hello, Ari!');
    });
});