import {describe, it, expect} from "bun:test";
import {User} from "user/user.ts";


describe('User Package', () => {
    it('should support user package', async () => {
        const user: User = new User('Ari');
        expect(user.name).toBe('Ari');
    });
});