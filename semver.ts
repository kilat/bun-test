const apiVersion = '2.1.0';

console.info(Bun.semver.satisfies(apiVersion, '1.x'));
console.info(Bun.semver.satisfies(apiVersion, '2.x'));
console.info(Bun.semver.satisfies(apiVersion, '>=1.0.0'));
console.info(Bun.semver.satisfies(apiVersion, '<2.0.0'));
console.info(Bun.semver.satisfies(apiVersion, '^1.1.1'));
