import {serve} from "bun";
import type {Routes} from "./src/interfaces/routes.interface.ts";
import type {RequestWithUser} from "./src/dto/users.dto.ts";
import {CustomError} from "./src/exceptions/CustomError.ts";

export type Middleware = (req: RequestWithUser, next: Function) => Promise<Response | void>;
type Handler = (req: RequestWithUser, params: Record<string, string>) => any;

export class Server {
    private routes: Routes[] = [];

    constructor(private port: number) {
        console.log('Server started')
    }

    public addRoute(method: string, pattern: string, handler: Handler, middlewares: Middleware[] = []) {
        this.routes.push({ method, pattern, handler, middlewares });
    }

    private matchRoute(method: string, pathname: string) {
        for (const route of this.routes) {
            if (route.method === method) {
                const paramNames: string[] = [];
                const regexPath = route.pattern.replace(/:(\w+)/g, (_, key) => {
                    paramNames.push(key);
                    return '([^/]+)';
                });

                const match = pathname.match(new RegExp(`^${regexPath}$`));
                if (match) {
                    const params = paramNames.reduce((acc, name, index) => {
                        acc[name] = match[index + 1];
                        return acc;
                    }, {} as Record<string, string>);
                    return { handler: route.handler, middlewares: route.middlewares, params };
                }
            }
        }
        return null;
    }

    private async executeMiddleware(req: RequestWithUser, middlewares: Middleware[], finalHandler: Handler, params: Record<string, string>): Promise<any> {
        let index = -1;

        async function next() {
            index++;
            if (index < middlewares.length) {
                return middlewares[index](req, next);
            } else {
                return finalHandler(req, params);
            }
        }

        return next();
    }

    public start() {
        serve({
            port: this.port,
            fetch: async (req: Request): Promise<Response> => {
                const url = new URL(req.url);
                const matchedRoute = this.matchRoute(req.method, url.pathname);

                if (matchedRoute) {
                    const { handler, middlewares, params } = matchedRoute;
                    const reqWithUser: RequestWithUser = req as RequestWithUser;
                    try {
                        return await this.executeMiddleware(reqWithUser, middlewares, handler, params);
                    } catch (error: any) {
                        if (error instanceof CustomError) {
                            return new Response(JSON.stringify(error), { status: error.status, headers: { 'Content-Type': 'application/json' } });
                        }
                        return new Response(JSON.stringify(new CustomError('Internal Server Error', 500)), { status: 500, headers: { 'Content-Type': 'application/json' } });
                    }
                } else {
                    return new Response(JSON.stringify(new CustomError('Not Found', 404)), { status: 404, headers: { 'Content-Type': 'application/json' } });
                }
            },
        });
    }
}