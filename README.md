# learningbun

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run index.ts
```

```bash
bun run --watch index.ts
```

To test

```bash
bun run index.ts
```

```bash
bun test --watch index.ts
```

This project was created using `bun init` in bun v1.1.10. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
